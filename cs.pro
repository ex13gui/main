SOURCES += main.cpp \
    mainwindow.cpp \
    test.cpp \
    progtab.cpp
FORMS += main.ui \
    test.ui \
    configform/configform.ui
HEADERS += mainwindow.h \
    test.h \
    progtab.h
QT += xml
OTHER_FILES += default.xml
