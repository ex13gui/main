# -------------------------------------------------
# Project created by QtCreator 2010-01-19T16:07:54
# -------------------------------------------------
TARGET = test
TEMPLATE = app
SOURCES += main.cpp \
    mywidget.cpp \
    treemodel.cpp \
    treeitem.cpp
HEADERS += mywidget.h \
    treemodel.h \
    treeitem.h
FORMS += mywidget.ui
