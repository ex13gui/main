/********************************************************************************
** Form generated from reading UI file 'mywidget.ui'
**
** Created: Tue Jan 19 19:32:13 2010
**      by: Qt User Interface Compiler version 4.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYWIDGET_H
#define UI_MYWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>
#include <QtGui/QTreeView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyWidget
{
public:
    QAction *exitAction;
    QAction *insertRowAction;
    QAction *insertColumnAction;
    QAction *removeRowAction;
    QAction *removeColumnAction;
    QAction *insertChildAction;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_2;
    QToolButton *toolBSave;
    QToolButton *toolBOpen;
    QSpacerItem *horizontalSpacer;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pushBRR;
    QPushButton *pushBIR;
    QPushButton *pushBIC;
    QPushButton *pushBRC;
    QPushButton *pushBNC;
    QTreeView *view;

    void setupUi(QWidget *MyWidget)
    {
        if (MyWidget->objectName().isEmpty())
            MyWidget->setObjectName(QString::fromUtf8("MyWidget"));
        MyWidget->resize(554, 330);
        exitAction = new QAction(MyWidget);
        exitAction->setObjectName(QString::fromUtf8("exitAction"));
        insertRowAction = new QAction(MyWidget);
        insertRowAction->setObjectName(QString::fromUtf8("insertRowAction"));
        insertColumnAction = new QAction(MyWidget);
        insertColumnAction->setObjectName(QString::fromUtf8("insertColumnAction"));
        removeRowAction = new QAction(MyWidget);
        removeRowAction->setObjectName(QString::fromUtf8("removeRowAction"));
        removeColumnAction = new QAction(MyWidget);
        removeColumnAction->setObjectName(QString::fromUtf8("removeColumnAction"));
        insertChildAction = new QAction(MyWidget);
        insertChildAction->setObjectName(QString::fromUtf8("insertChildAction"));
        verticalLayout = new QVBoxLayout(MyWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        frame = new QFrame(MyWidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        toolBSave = new QToolButton(frame);
        toolBSave->setObjectName(QString::fromUtf8("toolBSave"));

        horizontalLayout_2->addWidget(toolBSave);

        toolBOpen = new QToolButton(frame);
        toolBOpen->setObjectName(QString::fromUtf8("toolBOpen"));

        horizontalLayout_2->addWidget(toolBOpen);


        horizontalLayout->addWidget(frame);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        frame_2 = new QFrame(MyWidget);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_2);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        pushBRR = new QPushButton(frame_2);
        pushBRR->setObjectName(QString::fromUtf8("pushBRR"));

        horizontalLayout_3->addWidget(pushBRR);

        pushBIR = new QPushButton(frame_2);
        pushBIR->setObjectName(QString::fromUtf8("pushBIR"));

        horizontalLayout_3->addWidget(pushBIR);

        pushBIC = new QPushButton(frame_2);
        pushBIC->setObjectName(QString::fromUtf8("pushBIC"));

        horizontalLayout_3->addWidget(pushBIC);

        pushBRC = new QPushButton(frame_2);
        pushBRC->setObjectName(QString::fromUtf8("pushBRC"));

        horizontalLayout_3->addWidget(pushBRC);

        pushBNC = new QPushButton(frame_2);
        pushBNC->setObjectName(QString::fromUtf8("pushBNC"));

        horizontalLayout_3->addWidget(pushBNC);


        horizontalLayout->addWidget(frame_2);

        horizontalLayout->setStretch(1, 1);

        verticalLayout->addLayout(horizontalLayout);

        view = new QTreeView(MyWidget);
        view->setObjectName(QString::fromUtf8("view"));

        verticalLayout->addWidget(view);


        retranslateUi(MyWidget);
        QObject::connect(pushBIR, SIGNAL(clicked()), insertRowAction, SLOT(trigger()));
        QObject::connect(pushBIC, SIGNAL(clicked()), insertColumnAction, SLOT(trigger()));
        QObject::connect(pushBNC, SIGNAL(clicked()), insertChildAction, SLOT(trigger()));
        QObject::connect(pushBRC, SIGNAL(clicked()), removeColumnAction, SLOT(trigger()));
        QObject::connect(pushBRR, SIGNAL(clicked()), removeRowAction, SLOT(trigger()));

        QMetaObject::connectSlotsByName(MyWidget);
    } // setupUi

    void retranslateUi(QWidget *MyWidget)
    {
        MyWidget->setWindowTitle(QApplication::translate("MyWidget", "MyWidget", 0, QApplication::UnicodeUTF8));
        exitAction->setText(QApplication::translate("MyWidget", "exit", 0, QApplication::UnicodeUTF8));
        insertRowAction->setText(QApplication::translate("MyWidget", "insertRow", 0, QApplication::UnicodeUTF8));
        insertColumnAction->setText(QApplication::translate("MyWidget", "insertColumn", 0, QApplication::UnicodeUTF8));
        removeRowAction->setText(QApplication::translate("MyWidget", "removeRow", 0, QApplication::UnicodeUTF8));
        removeColumnAction->setText(QApplication::translate("MyWidget", "removeColumn", 0, QApplication::UnicodeUTF8));
        insertChildAction->setText(QApplication::translate("MyWidget", "insertChild", 0, QApplication::UnicodeUTF8));
        toolBSave->setText(QApplication::translate("MyWidget", "Save", 0, QApplication::UnicodeUTF8));
        toolBOpen->setText(QApplication::translate("MyWidget", "Open", 0, QApplication::UnicodeUTF8));
        pushBRR->setText(QApplication::translate("MyWidget", "RemoveRow", 0, QApplication::UnicodeUTF8));
        pushBIR->setText(QApplication::translate("MyWidget", "InsertRow", 0, QApplication::UnicodeUTF8));
        pushBIC->setText(QApplication::translate("MyWidget", "InsertClm", 0, QApplication::UnicodeUTF8));
        pushBRC->setText(QApplication::translate("MyWidget", "removeClm", 0, QApplication::UnicodeUTF8));
        pushBNC->setText(QApplication::translate("MyWidget", "NewChild", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MyWidget: public Ui_MyWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYWIDGET_H
