#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QtGui/QWidget>
#include <QModelIndex>

class QAction;
class QTreeView;
class QWidget;

namespace Ui
{
    class MyWidget;
}

class MyWidget : public QWidget
{
    Q_OBJECT

public:
    MyWidget(QWidget *parent = 0);
    ~MyWidget();

private:
    Ui::MyWidget *ui;

public slots:
    void updateActions();

private slots:
    void insertChild();
    bool insertColumn(const QModelIndex &parent = QModelIndex());
    void insertRow();
    bool removeColumn(const QModelIndex &parent = QModelIndex());
    void removeRow();
    void saveFile();
    void openFile();
};

#endif // MYWIDGET_H
