#include "mywidget.h"
#include "ui_mywidget.h"

#include <QtGui>
#include "treemodel.h"

MyWidget::MyWidget(QWidget *parent)
    : QWidget(parent), ui(new Ui::MyWidget)
{
    ui->setupUi(this);

    QStringList headers;
    headers << tr("Title") << tr("Description");

    QFile file("parameters.in");
    file.open(QIODevice::ReadOnly);
    TreeModel *model = new TreeModel(headers, file.readAll());
    file.close();

    ui->view->setModel(model);
    for (int column = 0; column < model->columnCount(); ++column)
        ui->view->resizeColumnToContents(column);

    connect(ui->exitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
    connect(ui->toolBOpen, SIGNAL(clicked()), this, SLOT(openFile()));
    connect(ui->toolBSave, SIGNAL(clicked()), this, SLOT(saveFile()));

    connect(ui->view->selectionModel(),
            SIGNAL(selectionChanged(const QItemSelection &,
                                    const QItemSelection &)),
            this, SLOT(updateActions()));

//    connect(ui->actionsMenu, SIGNAL(aboutToShow()), this, SLOT(updateActions()));
    connect(ui->insertRowAction, SIGNAL(triggered()), this, SLOT(insertRow()));
    connect(ui->insertColumnAction, SIGNAL(triggered()), this, SLOT(insertColumn()));
    connect(ui->removeRowAction, SIGNAL(triggered()), this, SLOT(removeRow()));
    connect(ui->removeColumnAction, SIGNAL(triggered()), this, SLOT(removeColumn()));
    connect(ui->insertChildAction, SIGNAL(triggered()), this, SLOT(insertChild()));

    updateActions();
}

MyWidget::~MyWidget()
{
    delete ui;
}

void MyWidget::insertChild()
{
    QModelIndex index = ui->view->selectionModel()->currentIndex();
    QAbstractItemModel *model = ui->view->model();



      QFile file("prova");
        QTextStream stream(&file);

        if (file.open(QFile::WriteOnly | QFile::Text)) {
            for (int row = 0; row < model->rowCount(QModelIndex()); ++row) {

                QStringList pieces;

                pieces.append(model->data(model->index(row, 0, QModelIndex()),
                                          Qt::DisplayRole).toString());
                pieces.append(model->data(model->index(row, 1, QModelIndex()),
                                          Qt::DisplayRole).toString());
                pieces.append(model->data(model->index(row, 0, QModelIndex()),
                                          Qt::DecorationRole).toString());

                stream << pieces.join(",") << "\n";
            }
        }

        file.close();









    if (model->columnCount(index) == 0) {
        if (!model->insertColumn(0, index))
            return;
    }

    if (!model->insertRow(0, index))
        return;

    for (int column = 0; column < model->columnCount(index); ++column) {
        QModelIndex child = model->index(0, column, index);
        model->setData(child, QVariant("[No data]"), Qt::EditRole);
        if (!model->headerData(column, Qt::Horizontal).isValid())
            model->setHeaderData(column, Qt::Horizontal, QVariant("[No header]"),
                                 Qt::EditRole);
    }

    ui->view->selectionModel()->setCurrentIndex(model->index(0, 0, index),
                                            QItemSelectionModel::ClearAndSelect);
    updateActions();
}

bool MyWidget::insertColumn(const QModelIndex &parent)
{
    QAbstractItemModel *model = ui->view->model();
    int column = ui->view->selectionModel()->currentIndex().column();

    // Insert a column in the parent item.
    bool changed = model->insertColumn(column + 1, parent);
    if (changed)
        model->setHeaderData(column + 1, Qt::Horizontal, QVariant("[No header]"),
                             Qt::EditRole);

    updateActions();

    return changed;
}

void MyWidget::insertRow()
{
    QModelIndex index = ui->view->selectionModel()->currentIndex();
    QAbstractItemModel *model = ui->view->model();

    if (!model->insertRow(index.row()+1, index.parent()))
        return;

    updateActions();

    for (int column = 0; column < model->columnCount(index.parent()); ++column) {
        QModelIndex child = model->index(index.row()+1, column, index.parent());
        model->setData(child, QVariant("[No data]"), Qt::EditRole);
    }
}

bool MyWidget::removeColumn(const QModelIndex &parent)
{
    QAbstractItemModel *model = ui->view->model();
    int column = ui->view->selectionModel()->currentIndex().column();

    // Insert columns in each child of the parent item.
    bool changed = model->removeColumn(column, parent);

    if (!parent.isValid() && changed)
        updateActions();

    return changed;
}

void MyWidget::removeRow()
{
    QModelIndex index = ui->view->selectionModel()->currentIndex();
    QAbstractItemModel *model = ui->view->model();
    if (model->removeRow(index.row(), index.parent()))
        updateActions();
}

void MyWidget::updateActions()
{
    bool hasSelection = !ui->view->selectionModel()->selection().isEmpty();
    ui->removeRowAction->setEnabled(hasSelection);
    ui->removeColumnAction->setEnabled(hasSelection);

    bool hasCurrent = ui->view->selectionModel()->currentIndex().isValid();
    ui->insertRowAction->setEnabled(hasCurrent);
    ui->insertColumnAction->setEnabled(hasCurrent);

    if (hasCurrent) {
        ui->view->closePersistentEditor(ui->view->selectionModel()->currentIndex());

        int row = ui->view->selectionModel()->currentIndex().row();
        int column = ui->view->selectionModel()->currentIndex().column();
//        if (ui->view->selectionModel()->currentIndex().parent().isValid())
//            statusBar()->showMessage(tr("Position: (%1,%2)").arg(row).arg(column));
//        else
//            statusBar()->showMessage(tr("Position: (%1,%2) in top level").arg(row).arg(column));
    }
}

void MyWidget::openFile()
{
    QString fileName;
    QAbstractItemModel *model = ui->view->model();
    QStringList headers;
    headers << tr("Variable") << tr("Value");
    fileName = QFileDialog::getOpenFileName(this, tr("Choose a data file"),
                                                "parameters", "*.*");


    if (!fileName.isEmpty()) {
        QFile file(fileName);

        if (file.open(QFile::ReadOnly | QFile::Text)) {
            QTextStream stream(&file);
            QString line;

            model->removeRows(0, model->rowCount(QModelIndex()), QModelIndex());

            file.open(QIODevice::ReadOnly);
            TreeModel *model2 = new TreeModel(headers, file.readAll());
             ui->view->setModel(model2);
            file.close();


//             int row = 0;
//   do {
//                 line = stream.readLine();
//                 if (!line.isEmpty()) {
//
//                     model->insertRows(row, 1, QModelIndex());
//
//                     QStringList pieces = line.split(" ", QString::SkipEmptyParts);
// 		    //                    model->setData(model->index(row, 0, QModelIndex()),
// 		    //             pieces.value(1));
// 		    // model->setData(model->index(row, 1, QModelIndex()),
// 		    //             pieces.value(2));
// 		    //  model->setData(model->index(row, 1, QModelIndex()),
// 		    //           QColor(pieces.value(3)), Qt::DecorationRole);                    row++;
//                 }
//             } while (line != "start1");
//
//
//             do {
//                 line = stream.readLine();
//                 if (!line.isEmpty()) {
//
//                     model->insertRows(row, 1, QModelIndex());
//
//                     QStringList pieces = line.split(" ", QString::SkipEmptyParts);
//                     model->setData(model->index(row, 0, QModelIndex()),
//                                    pieces.value(1));
//                     model->setData(model->index(row, 1, QModelIndex()),
//                                    pieces.value(2));
// 		    model->setData(model->index(row, 1, QModelIndex()),
// 		                 QColor(pieces.value(3)), Qt::DecorationRole);
//                     row++;
//                 }
//             } while (line != "endl");

//             file.close();
//            statusBar()->showMessage(tr("Loaded %1").arg(fileName), 2000);
        }
    }
}

void MyWidget::saveFile()
{
  QAbstractItemModel *model = ui->view->model();
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save file as"), "parameters", "*.in");

    if (!fileName.isEmpty()) {
        QFile file(fileName);
        QTextStream stream(&file);

        if (file.open(QFile::WriteOnly | QFile::Text)) {
            for (int row = 0; row < model->rowCount(QModelIndex()); ++row) {

                QStringList pieces;
                QModelIndex topIndex = model->index(row, 0, QModelIndex());
                pieces.append(model->data(topIndex,Qt::DisplayRole).toString());
                int crows = model->rowCount(topIndex);
                stream << pieces.join("\t") << "\n";
                for (int i=0; i < crows; i++) {
                  QStringList piecesc;
                  QModelIndex secondLevelIndex = model->index(i, 0, topIndex);
                  piecesc.append(model->data(secondLevelIndex,Qt::DisplayRole).toString());
                  QModelIndex secondLevelValue = model->index(i, 1, topIndex);
                  piecesc.append(model->data(secondLevelValue,Qt::DisplayRole).toString());
                  stream << "  " << piecesc.join("\t") << "\n";
                }

/*        // check top row
    QModelIndex topIndex = model->index(0, 0, QModelIndex());
    int rows = model->rowCount(topIndex);
    Q_ASSERT(rows >= 0);
    if (rows > 0)
        Q_ASSERT(model->hasChildren(topIndex) == true);

    QModelIndex secondLevelIndex = model->index(0, 0, topIndex);
    if (secondLevelIndex.isValid()) { // not the top level
        // check a row count where parent is valid
        rows = model->rowCount(secondLevelIndex);
        Q_ASSERT(rows >= 0);
        if (rows > 0)
            Q_ASSERT(model->hasChildren(secondLevelIndex) == true);
    }*/




            }
        }

        file.close();
        //statusBar()->showMessage(tr("Saved %1").arg(fileName), 2000);
    }
}
