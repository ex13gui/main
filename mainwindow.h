#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore>
#include <QtGui>
#include <QtXml>
#include "ui_main.h"

#include "progtab.h"
#include <map>

class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

public:

    std::map<QString, ProgTab> tablist;

    MainWindow(QWidget *parent = 0);

    void readXmlDocument();

public slots:

    void RunModule();
    void DocModule();

private slots:
    void moduleFinished();
    void moduleError(QProcess::ProcessError error);
    void on_treeWidget_currentItemChanged(QTreeWidgetItem* ,QTreeWidgetItem* );
    void on_treeWidget_expanded();
};

#endif // MAINWINDOW_H
