#ifndef PROGTAB_H
#define PROGTAB_H

//#include <QtCore>
//#include <QtGui>
#include <QtDebug>

class ProgTab
{

public:
    QString name; // module gui name (mandatory)
    QString run;  // executing command (optional)
    QString doc;  // documentation (opt)
    QString dir;  // executing directory (opt, default launching dir)
    QString det;  // run as detached (yes to detach, default no)

    ProgTab();
    ~ProgTab(){};

//    void execute(QObject *,QProcess*);
    void print();

};

#endif // PROGTAB_H
