#ifndef TEST_H
#define TEST_H

#include <QtGui>
#include "ui_test.h"

class TestWidget : public QWidget, private Ui::Form
{
    Q_OBJECT

public:
    TestWidget(QWidget *parent = 0);

public slots:
    void outRead();
    void clearConsole();
};

#endif // TEST_H
