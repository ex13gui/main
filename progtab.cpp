#include "progtab.h"

ProgTab::ProgTab()
{
    name = "";
    run = "";
    doc = "";
    dir = "";
    det = "";
}

void ProgTab::print()
{
    qDebug() << "--------";
    qDebug() << "module: ";
    qDebug() << this->name;
    qDebug() << this->run;
    qDebug() << this->doc;
    qDebug() << this->dir;
    qDebug() << this->det;
}

//void ProgTab::execute(QObject *rootp,QProcess* childp)
//{
//    QProcess *process = new QProcess(rootp);
//    childp = process;
//    process->setProcessChannelMode(QProcess::MergedChannels);
//    QObject::connect(process, SIGNAL(finished(int)), rootp, SLOT(moduleFinished()));
//    QObject::connect(process, SIGNAL(error(QProcess::ProcessError)), rootp, SLOT(moduleError(QProcess::ProcessError)));
//    QObject::connect(process, SIGNAL(readyRead()), rootp, SLOT(outRead()));
//
//    if(!((this->dir).isEmpty())) process->setWorkingDirectory(this->dir);
//    process->start(this->run);
//}
