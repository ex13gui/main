#include "test.h"

#include <QtGui>

TestWidget::TestWidget(QWidget *parent)
    : QWidget(parent)
{
    setupUi(this);
}

void TestWidget::outRead()
{
    QProcess *pr = (QProcess*)sender();

    QByteArray newData = pr->readLine();
    QString text = consoleWindow->toPlainText()
                   + QString::fromLocal8Bit(newData);
    consoleWindow->setPlainText(text);
}

void TestWidget::clearConsole()
{
    consoleWindow->clear();
}
