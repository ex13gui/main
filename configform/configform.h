#ifndef CONFIGFORM_H
#define CONFIGFORM_H

#include <QtGui/QWidget>

class QWidget;

namespace Ui
{
    class ConfigForm;
}

class ConfigForm: public QWidget
{
    Q_OBJECT

public:
    ConfigForm(QWidget *parent = 0);
    ~ConfigForm();
    void setupUi();

private:
    Ui::ConfigForm *ui;

// public slots:
    

// private slots:
    
};

#endif // CONFIGFORM_H
