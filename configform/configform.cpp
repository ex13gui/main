#include "configform.h"
#include "ui_configform.h"

#include <QtGui>

ConfigForm::ConfigForm(QWidget *parent)
    : QWidget(parent), ui(new Ui::ConfigForm)
{
//    ui->setupUi(this);
    setupUi();
}

ConfigForm::~ConfigForm()
{
    delete ui;
}

void ConfigForm::setupUi()
{
    qDebug() << "prova";
    ui->Ui::ConfigForm::setupUi(this);

    QCheckBox *new_checkBox;
    QLineEdit *new_lineEdit;

    new_checkBox = new QCheckBox(this);
    new_checkBox->setObjectName(QString::fromUtf8("new_opt"));
    new_checkBox->setChecked(true);
    this->ui->gridLayout->addWidget(new_checkBox, 2, 0, 1, 1);
    new_lineEdit = new QLineEdit(this);
    new_lineEdit->setObjectName(QString::fromUtf8("new_lineEdit"));
    this->ui->gridLayout->addWidget(new_lineEdit, 2, 1, 1, 1);


};
//
//public:
//    QGridLayout *gridLayout;
//    QCheckBox *checkBox;
//    QLineEdit *lineEdit;
//    QCheckBox *checkBox_2;
//    QLineEdit *lineEdit_2;
//
//    void setupUi(QWidget *ConfigForm)
//    {
//        if (ConfigForm->objectName().isEmpty())
//            ConfigForm->setObjectName(QString::fromUtf8("ConfigForm"));
//        ConfigForm->resize(400, 300);
//        gridLayout = new QGridLayout(ConfigForm);
//        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
//        checkBox = new QCheckBox(ConfigForm);
//        checkBox->setObjectName(QString::fromUtf8("checkBox"));
//        checkBox->setChecked(true);
//        checkBox->setTristate(false);
//
//        gridLayout->addWidget(checkBox, 0, 0, 1, 1);
//
//        lineEdit = new QLineEdit(ConfigForm);
//        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
//
//        gridLayout->addWidget(lineEdit, 0, 1, 1, 1);
//
//        checkBox_2 = new QCheckBox(ConfigForm);
//        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));
//
//        gridLayout->addWidget(checkBox_2, 1, 0, 1, 1);
//
//        lineEdit_2 = new QLineEdit(ConfigForm);
//        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
//
//        gridLayout->addWidget(lineEdit_2, 1, 1, 1, 1);
//
//
//        retranslateUi(ConfigForm);
//
//        QMetaObject::connectSlotsByName(ConfigForm);
//    } // setupUi
