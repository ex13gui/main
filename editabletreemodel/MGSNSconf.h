#ifndef __mgsnsc_h__
#define __mgsnsc_h__

//boundary integration
#define BOUNDARYNS 1 
 // ===========================
 //  Physics
 // ===========================

// =================================================
// density=density of (t,x[],phase,temperature,etc..)
// =======================================

// if  CONST is not defined then no inline functions
#define CONST 1
#ifndef CONST 
// nondimensional density (rho/rhoref)
inline double adensity(double Temp){return 1.;}
// nondimensional viscosity (mu/muref)
inline double aviscosity(double Temp){return .1;}
#endif




// 3D NAVIER-STOKES ADVECTION term
  #define ADV 	0.
  #define ADV1 	0.
  #define STAB 	0.


// MULTIGRID PARAMETERS
  #define  MaxIter_NS 	15
  #define Eps_NS 	1.e-6
  #define MLSolverId_NS 	MGIterId
  #define NestedMG_NS	  0
  #define Gamma_NS 	2
  #define NoMGIter_NS 	1
// Multigrid 
  #define RestrType	 1   
  #define  Nu1_NS	 8  
  #define Nu2_NS	 8
  #define Omega_NS	 0.98
  #define SOLVPROC_NS	   GMRESIter
  #define PRECPROC_NS 	  ILUPrecond
// Coarse Grid 
  #define  NuC_NS	  40 
  #define OmegaC_NS	  0.
  #define SOLVPROCC_NS 	 GMRESIter
  #define PRECPROCC_NS	  ILUPrecond


// **********************************************************

// strings ============================
#ifdef DIM2
#define MRL 59
#else
#define MRL 402
#endif

#endif
