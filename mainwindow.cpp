#include "mainwindow.h"

#include <QtGui>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setupUi(this);
    this->readXmlDocument();
}

void MainWindow::readXmlDocument()
{
    QDomDocument *doc = new QDomDocument();

    QFile file("default.xml");

    doc->setContent( &file );

    QDomElement root = doc->documentElement();

    QTreeWidgetItem *itemroot = new QTreeWidgetItem(treeWidget);
    itemroot->setText(0, root.tagName());
    itemroot->setText(1, root.attribute("name"));

    QDomNode n = root.firstChild();
    while( !n.isNull() )
    {
      QDomElement e = n.toElement();
        if( !e.isNull() )
            {
            ProgTab p ;
            p.name = e.attribute("name");
            p.run = e.attribute("run");
            p.doc = e.attribute("doc");
            p.dir = e.attribute("dir");
            p.det = e.attribute("det");
            p.print();
            tablist[p.name] = p;
            QTreeWidgetItem *itemchild = new QTreeWidgetItem(itemroot);
            itemchild->setText(0, e.attribute("name"));
            itemchild->setText(1, e.text());
            }
          n = n.nextSibling();
    }

}

void MainWindow::on_treeWidget_currentItemChanged(QTreeWidgetItem* current,QTreeWidgetItem* previous)
{
    ProgTab prog = tablist[current->data(0,Qt::DisplayRole).toString()];
    pushButtonRun->setEnabled(!prog.run.isEmpty());
    pushButtonDoc->setEnabled(!prog.doc.isEmpty());
}

void MainWindow::on_treeWidget_expanded()
{
    treeWidget->resizeColumnToContents ( 0 );
    treeWidget->resizeColumnToContents ( 1 );
}

void MainWindow::RunModule()
{
    QString curitem = treeWidget->currentItem()->data(0,Qt::DisplayRole).toString();
    ProgTab prog = tablist[curitem];
    QProcess *moduleproc = new QProcess(this);
    moduleproc->setProcessChannelMode(QProcess::MergedChannels);
    connect(moduleproc, SIGNAL(finished(int)), this, SLOT(moduleFinished()));
    connect(moduleproc, SIGNAL(error(QProcess::ProcessError)), this, SLOT(moduleError(QProcess::ProcessError)));
    connect(moduleproc, SIGNAL(readyRead()), moduleWidget, SLOT(outRead()));

    if(!prog.dir.isEmpty()) moduleproc->setWorkingDirectory(prog.dir);
    if(!prog.det.compare("yes")) {
        moduleproc->startDetached(prog.run);
        statusBar()->showMessage(tr("Module started in detached process..."), 5000);
//        qDebug() << "starting detatched";
    }
    else {
        moduleproc->start(prog.run);
//        qDebug() << "starting attached";
    }

}

void MainWindow::DocModule()
{
    statusBar()->showMessage(tr("not yet implemented..."), 5000);
}

void MainWindow::moduleFinished()
{
    qDebug() << "MainWindow::moduleFinished";
    statusBar()->showMessage(tr("External module execution finished..."), 5000);
}

void MainWindow::moduleError(QProcess::ProcessError error)
{
    qDebug() << "MainErrorWindow::moduleError";
    if (error != QProcess::Crashed)
        QMessageBox::critical(0, tr("Failed to launch the module"),
                          tr("Could not launch the module. Check xml config file."),
                          QMessageBox::Cancel);
    statusBar()->showMessage(tr("External module execution error..."), 5000);
}


